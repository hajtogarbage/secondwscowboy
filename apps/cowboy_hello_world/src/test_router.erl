-module(test_router).

-export([init/2]).

init(Req,Opts) ->
  Reply = cowboy_req:reply(200,
                          [{<<"content-type">>, <<"text/plain">>}],
                           <<"Test Route!">>,
                           Req),
  {ok, Reply, Opts}.
